package com.bending.bender.bachclientpa.test;


import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.support.v4.view.ViewPager;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import com.bending.bender.bachclientpa.BashBaseAdapter;
import com.bending.bender.bachclientpa.HtmlParse;
import com.bending.bender.bachclientpa.MyActivity;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by Bender on 17.10.2014.
 */
public class MainActivitytest extends ActivityInstrumentationTestCase2<MyActivity> {

    /**
     * MyActivity test object
     */
    MyActivity mMainTest;
    /**
     * ViewPager in MyActivity to test
     */
    ViewPager mainPager;
    /**
     * Reference to jokesDB
     */
    BashBaseAdapter jokesDB;
    /**
     * Selected section
     */
    public int view_section;
    /**
     * Current page
     */
    public int current_page;

    public MainActivitytest() {
        super(MyActivity.class);
    }

    /**
     * Initialize test variables
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mMainTest = getActivity();
        mainPager=mMainTest.mainpager;
        jokesDB=mMainTest.jokesDB;
        view_section=mMainTest.view_section;
        current_page=mMainTest.current_page;
    }

    /**
     * Check test variables not null
     */
    public void testPreconditions() {
        //Try to add a message to add context to your assertions. These messages will be shown if
        //a tests fails and make it easy to understand why a test failed
        assertNotNull(mMainTest);
        assertNotNull(mainPager);
        assertNotNull(jokesDB);
        assertNotNull(view_section);
        assertNotNull(current_page);
    }

    /**
     * Method that tests savelastnewpage() method of myActivity class
     */
   public void testsavelastnewpage(){
        int testPageNumber=955;
        int testNumber=0;
        mMainTest.savelastnewpage(testPageNumber);
        SharedPreferences sPref=mMainTest.getPreferences(mMainTest.MODE_PRIVATE);
        assertNotNull(sPref);
        if (sPref!=null)
            testNumber=sPref.getInt("NEW_PAGE_NUMBER",0);

        assertEquals(testPageNumber,testNumber);
    }
    /**
     * Method that tests savelastratingpage() method of MyActivity class
     */
    public void testSavelastratingpage(){
        int testPageNumber=3;
        int testNumber=0;
        mMainTest.savelastratingpage(testPageNumber);
        SharedPreferences sPref=mMainTest.getPreferences(mMainTest.MODE_PRIVATE);
        assertNotNull(sPref);
        if (sPref!=null)
            testNumber=sPref.getInt("RATING_PAGE_NUMBER",0);

        assertEquals(testPageNumber,testNumber);
    }

    /**
     * Method that tests loadnewpage() method of MyActivity class
     */
    public void testLoadnewPage(){
        SharedPreferences sPref;
        int testPageNumber=145;
        sPref = mMainTest.getPreferences(mMainTest.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putInt("NEW_PAGE_NUMBER", testPageNumber);
        ed.commit();
        assertEquals(testPageNumber,mMainTest.loadnewpage());
    }
    /**
     * Method that tests loadratingpage() method of MyActivity class
     */
    public void testLoadratingpage(){
        SharedPreferences sPref;
        int testPageNumber=8;
        sPref = mMainTest.getPreferences(mMainTest.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putInt("RATING_PAGE_NUMBER", testPageNumber);
        ed.commit();
        assertEquals(testPageNumber,mMainTest.loadratingpage());
    }

    /**
     * Test visibility of loader and pager of Activity
     */
    public void testControlsVisible() {
        ViewAsserts.assertOnScreen(mainPager.getRootView(), mainPager);

    }

    /**
     * Tests the screen orientation change
     */
     public void testOrientationChange(){
        int testCurrentPage=current_page;
        int testViewSection=view_section;
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        assertEquals(testCurrentPage,mMainTest.current_page);
        assertEquals(testViewSection,mMainTest.view_section);
       getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        assertEquals(testCurrentPage,mMainTest.current_page);
        assertEquals(testViewSection,mMainTest.view_section);
    }
    /**
     * HtmlParse test
     */
    public void testHtmlParse() throws IOException{
        final String testUrl = "http://www.bash.im/index";
        new Thread(new Runnable() {
            public void run() {
                try {
                    Document doc = Jsoup.connect(testUrl).get();
                    HtmlParse htmlParse = new HtmlParse();
                    htmlParse.Parse(doc);
                    for (int i = 0; i < htmlParse.size(); i++) {
                        assertNotNull(htmlParse.getText().get(i));
                        assertNotNull(htmlParse.getDate().get(i));
                        assertNotNull(htmlParse.getComics().get(i));
                        assertNotNull(htmlParse.getRating().get(i));
                        assertNotNull(htmlParse.getId().get(i));
                    }
                } catch (IOException e) {
                    fail("Connection error");
                }
            }
        });
    }

    /**
     * Tests BashBaseAdapter
     * @throws Exception
     */
    public void testBashBaseAdapter()throws Exception{
        String testId="444444";
        String testText="TestTextQuote";
        String testRating="777";
        String testComics="";
        String testDate="2014-11-06 11:13";
        int testSection=mMainTest.SECTION_NEW;
        int testPageNumber=123;

        BashBaseAdapter adapter=new BashBaseAdapter(mMainTest);
        assertNotNull(adapter.getmDB());
        //add test jokes
        adapter.addJoke(testText,testDate,testId,testRating,testComics,testSection,testPageNumber);
        Cursor cursor=adapter.getData(testSection,testPageNumber);
        assertTrue(cursor.moveToFirst());
        //delete test jokes
        adapter.delJokes(testSection,testPageNumber);
        cursor=adapter.getData(testSection,testPageNumber);
        assertFalse(cursor.moveToFirst());
    }
}
