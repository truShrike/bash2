package com.bending.bender.bachclientpa;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * * Class for working with database
 */

public class BashBaseAdapter {
	  /************Jokes Table Fields ************/

    public static final String KEY_JOKE_TEXT = "JokeText";
    public static final String KEY_JOKE_DATE_TIME = "JokeDT";
    public static final String KEY_JOKE_ID = "JokeiD";
    public static final String KEY_JOKE_RATING = "JokeRating";
    public static final String KEY_JOKE_COMICS = "JokeComics";
    public static final String KEY_JOKE_SECTION = "JokeSection";
    public static final String KEY_JOKE_PAGE_NUMBER = "JokePage";
    /************* Database Name ************/
    public static final String DATABASE_NAME = "BASH_DB_sqllite";
     
    /**** Database Version (Increase one if want to also upgrade your database) ****/
    public static final int DATABASE_VERSION = 1;// started at 1
 
    /** Table names */
    public static final String BASH_JOKES = "bashJokes";
    /** Create table syntax */
    private static final String BASH_JOKES_CREATE = "create table bashJokes" +
    										 "( _id integer primary key autoincrement,JokeText text," +
    										 " JokeDT text,JokeiD tex,JokeRating text,JokeComics text,JokeSection integer,JokePage integer);";

    
    private final Context mCtx;
    
    
    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;
    public SQLiteDatabase getmDB(){
        return mDB;
    }
    /**
     * BashBaseAdapter class constructor
     * @param ctx
     */
    public BashBaseAdapter(Context ctx) {
      mCtx = ctx;
        mDBHelper = new DBHelper(mCtx, DATABASE_NAME, null, DATABASE_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }


    /**
     * Get jokes from database
     * @param Section - section
     * @param PageNumber page number
     * @return
     */
    public Cursor getData(int Section, int PageNumber) {
        if ((Section==1) || (Section==5)) {
            return mDB.query(BASH_JOKES, null, KEY_JOKE_SECTION + "=" + Integer.toString(Section)+
                    " AND "+KEY_JOKE_PAGE_NUMBER+"="+Integer.toString(PageNumber), null, null, null, null);

        }
        else {
            return mDB.query(BASH_JOKES, null, KEY_JOKE_SECTION + "=" + Integer.toString(Section), null, null, null, null);
        }

    }

    /**  Add Record to database
     * @param jtext Joke text
     * @param jdt  joke creation time
     * @param jn   joke id
     * @param jr   joke rating
     * @param jc   joke comics link
     * @param js   joke section
     * @param jpn  joke page number
     */
    public  void addJoke(String jtext,String jdt,String jn,String jr,String jc,int js,int jpn) {
	 	   
          ContentValues cVal = new ContentValues();
          cVal.put(KEY_JOKE_TEXT, jtext);
          cVal.put(KEY_JOKE_DATE_TIME, jdt);
          cVal.put(KEY_JOKE_ID, jn);
          cVal.put(KEY_JOKE_RATING, jr);
          cVal.put(KEY_JOKE_COMICS, jc);
          cVal.put(KEY_JOKE_SECTION, js);
          cVal.put(KEY_JOKE_PAGE_NUMBER, jpn);
          mDB.insert(BASH_JOKES, null, cVal);
         
      
    }

    /**
     * Delete jokes from database
     * @param Section
     * @param PageNumber
     */
    public void delJokes(int Section,int PageNumber) {

            if ((Section==MyActivity.SECTION_NEW) || (Section==MyActivity.SECTION_RATING)) {
                mDB.delete(BASH_JOKES, KEY_JOKE_SECTION + "=" + Section+" AND "+KEY_JOKE_PAGE_NUMBER+" > "+(PageNumber+4),null);
                mDB.delete(BASH_JOKES, KEY_JOKE_SECTION + "=" + Section+" AND "+KEY_JOKE_PAGE_NUMBER+" < "+(PageNumber-4),null);
                mDB.delete(BASH_JOKES, KEY_JOKE_SECTION + "=" + Section+" AND "+KEY_JOKE_PAGE_NUMBER+" = "+(PageNumber),null);
            }
            else
            {
                mDB.delete(BASH_JOKES, KEY_JOKE_SECTION + "=" + Integer.valueOf(Section), null);
            }
	    }

    /**
     *  db helper
     *  class helper for database create and control
     */
    private class DBHelper extends SQLiteOpenHelper {

      public DBHelper(Context context, String name, CursorFactory factory,
          int version) {
        super(context, name, factory, version);
      }

    
      @Override
      /**
       *  Create and fill table of database
       *  @param db - SQLiteDatabase
       */
      public void onCreate(SQLiteDatabase db) {
        db.execSQL(BASH_JOKES_CREATE);

      }
        /**
         * Update database
         * @param db - database to update
         * @param oldVersion - old version number
         * @param newVersion - new version number
         */
      @Override
      public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
          db.execSQL("DROP TABLE IF EXISTS "+ BASH_JOKES);
          onCreate(db);
      }
    }
    
	
}// BashBaseAdapter