package com.bending.bender.bachclientpa;

import android.app.PendingIntent;
import android.app.Service;

import android.content.Intent;
import android.os.IBinder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;






public class GetData extends Service {

    /**
     * Database adapter
     */
    BashBaseAdapter jokesDBS;
    /**
     * Elements for parsing jokes
     */
    Elements eQuotes;
    /**
     * Selected Section
     */
    int Section;
    int Direction;
    int iCP;
    String smainurl;
    String  sNP;
    Document doc;
    boolean bgoodjob;
    PendingIntent pi;
    public void onCreate() {
        super.onCreate();

    }

    /**
     * Prepearing parsing page task
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    public int onStartCommand(Intent intent, int flags, int startId) {

        MyGlobal myApp=(MyGlobal)this.getApplication();
        myApp.setstate(true);
         jokesDBS = new BashBaseAdapter(getApplicationContext() );
         Section = intent.getIntExtra(MyActivity.PARAM_SECTION, 1);
         Direction = intent.getIntExtra(MyActivity.PARAM_BUTTON, 1);
         iCP = intent.getIntExtra(MyActivity.PARAM_CURRENT_PAGE, 1);
         sNP="0";

         pi = intent.getParcelableExtra(MyActivity.PARAM_PINTENT);
        smainurl=getResources().getString(R.string.Bash_url);
        switch(Section)
        {
            case MyActivity.SECTION_NEW:smainurl+="/index/";break;
            case MyActivity.SECTION_RANDOM:smainurl+="/random";break;
            case MyActivity.SECTION_BEST:smainurl+="/best";break;
            case MyActivity.SECTION_ABYSS:smainurl+="/abyss";break;
            case MyActivity.SECTION_RATING:smainurl+="/byrating/";break;
        }
        if ((Section==MyActivity.SECTION_NEW)|| (Section==MyActivity.SECTION_RATING)) {
            switch (Direction) {

                case MyActivity.PREVIOUS_PAGE:{ iCP--;smainurl = smainurl + String.valueOf(iCP);} break;
                case MyActivity.NEXT_PAGE: {iCP++;smainurl = smainurl + String.valueOf(iCP);}  break;
                case MyActivity.REFRESH:{smainurl = smainurl + String.valueOf(iCP);}break;
            }
        }
        ParsingTask();


        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();

    }

    public IBinder onBind(Intent intent) {
        return null;
    }


    /**
     * Parsing selected page task
     */
    void ParsingTask( ) {
        bgoodjob=false;
        new Thread(new Runnable() {
            public void run() {
            try{
                try {


                    doc = Jsoup.connect(smainurl).get();
                    if (doc!=null) {
                        if ((Section==MyActivity.SECTION_NEW)||(Section==MyActivity.SECTION_RATING))
                        {
                            Elements etemp = doc.select("[class=page]");
                            sNP = etemp.attr("value");
                        }
                        else
                        {
                            sNP="0";
                        }
                        eQuotes = doc.select(".quote");

                        Elements id, text, date, rating, comics;

                        jokesDBS.delJokes(Section,Integer.valueOf(sNP));

                        for (Element Quotes : eQuotes) {

                            id = Quotes.select("a[class=id]");
                            text = Quotes.select(".text");
                            date = Quotes.select(".date");
                            rating = Quotes.select(".rating");
                            comics = Quotes.select("a[class=comics]");
                            String temp = id.text();
                            temp = temp.replaceAll("#", "");
                            if (text.text() != "") {
                                jokesDBS.addJoke(text.text(), date.text(),
                                        temp, rating.text(), comics.attr("href"),Section,Integer.valueOf(sNP));
                            }
                        }
                        bgoodjob=true;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


                Intent intent = new Intent();

                intent.putExtra(MyActivity.PARAM_SECTION, Section);
                if (bgoodjob==true) {
                    intent.putExtra(MyActivity.PARAM_CURRENT_PAGE, Integer.valueOf(sNP));
                    pi.send(GetData.this, MyActivity.STATUS_RDONE, intent);
                }
                else {
                    intent.putExtra(MyActivity.PARAM_CURRENT_PAGE, Integer.valueOf(iCP));
                    pi.send(GetData.this, MyActivity.STATUS_RFAULT, intent);
                }


            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }

                stopSelf();
            }
        }).start();
    }

   }