package com.bending.bender.bachclientpa;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by batisto4ka on 07.11.2014.
 */
public class HtmlParse {
    ArrayList<String> id;
    ArrayList<String> text;
    ArrayList<String> date;
    ArrayList<String> rating;
    ArrayList<String> comics;

    int size;

    public ArrayList<String> getId()    {return id;}
    public ArrayList<String> getText()  {return text;}
    public ArrayList<String> getDate()  {return date;}
    public ArrayList<String> getRating(){return rating;}
    public ArrayList<String> getComics(){return comics;}
    public int size(){return size;}

    public HtmlParse(){}
    public void Parse(Document doc) {
        if (doc != null) {
            Elements eQuotes = doc.select(".quote");
            Elements id;
            for (Element Quotes : eQuotes) {
                //id.add(Quotes.select("a[class=id]").text());
                id=Quotes.select("a[class=id]");
                String sid=id.text();
                sid=sid.replaceAll("#", "");
                this.id.add(sid);
                text.add(Quotes.select(".text").text());
                date.add(Quotes.select(".date").text());
                rating.add(Quotes.select(".rating").text());
                comics.add(Quotes.select("a[class=comics]").attr("href"));
            }
            size=this.id.size();
        }
    }
}
