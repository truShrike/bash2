package com.bending.bender.bachclientpa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;




public class ImageActivity extends Activity {
	
	private String simagelink;
	

	 
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.ivcomics);
	    
	    Intent intent = getIntent();
	    simagelink = intent.getStringExtra("imagelink");
	    ImageView imgView = (ImageView) findViewById(R.id.imageViewComics);
	    Picasso.with(this).load(simagelink).into(imgView);

	    
	     
	  }

}
