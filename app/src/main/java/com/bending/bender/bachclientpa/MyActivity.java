package com.bending.bender.bachclientpa;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MyActivity  extends FragmentActivity {


    public static final byte SECTION_NEW = 1;
    public static final byte SECTION_RANDOM = 2;
    public static final byte SECTION_BEST = 3;
    public static final byte SECTION_ABYSS = 4;
    public static final byte SECTION_RATING = 5;
    public static final byte SECTION_DOWNLOAD = 6;

    public static final byte MAIN_PAGE = 0;
    public static final byte PREVIOUS_PAGE = 1;
    public static final byte NEXT_PAGE = 2;
    public static final byte REFRESH = 3;


    static final int PAGE_COUNT = 2;
    public final static String PARAM_SECTION = "Section";
    public final static String PARAM_CURRENT_PAGE = "CurrentPage";
    public final static String PARAM_BUTTON = "Button";
    public final static int STATUS_RDONE = 111;
    public final static int STATUS_RFAULT = 222;
    public final static String PARAM_PINTENT = "pendingIntent";
    public ViewPager mainpager;
    PagerAdapter pagerAdapter;
    /**
     * Cache database
     */
   public  BashBaseAdapter jokesDB;
    /**
     *
     */
    public String nextpage;
    /**
     * Selected section
     */
    public int view_section;
    /**
     * Current page
     */
    public int current_page;



    @Override
    /**
     * Activity on Create method
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        jokesDB = new BashBaseAdapter(this);
        nextpage = "0";

        mainpager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),SECTION_NEW,loadnewpage());
        mainpager.setAdapter(pagerAdapter);


        if (savedInstanceState == null) {

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork!=null) {
                PendingIntent pi;
                Intent intent = new Intent(this, GetData.class);
                pi = createPendingResult(1, intent, 0);
                intent.putExtra(PARAM_SECTION, SECTION_NEW).putExtra(PARAM_CURRENT_PAGE, 0).putExtra(PARAM_BUTTON, MAIN_PAGE).putExtra(PARAM_PINTENT, pi);
                startService(intent);
            }
            else
            {
                Toast.makeText(this, getResources().getString(R.string.toast_no_connection), Toast.LENGTH_SHORT).show();
            }

        }


    }

    /**
     * Check internet connection
     * @return true- if connection presents, false -if no connection
     */
    public  boolean check_connection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null)
            return true;
        else return false;
    }

    /**
     * Saving last viewed page in new section
     * @param PageNumber last viewed page
     */
    public void savelastnewpage(int PageNumber)
    {
        SharedPreferences sPref;
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putInt("NEW_PAGE_NUMBER", PageNumber);
        ed.commit();

    }

    /**
     * Saving last viewed page in By rating section
     * @param PageNumber last viewed page
     */
    public void savelastratingpage(int PageNumber)
    {
        SharedPreferences sPref;
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putInt("RATING_PAGE_NUMBER", PageNumber);
        ed.commit();

    }

    /**
     *  Loading last viewed page in new section
     * @return page number
     */
    public int loadnewpage() {
        SharedPreferences sPref;
        sPref = getPreferences(MODE_PRIVATE);
        return sPref.getInt("NEW_PAGE_NUMBER", 0);


    }
    /**
     *  Loading last viewed page in by rating section
     * @return page number
     */
    public int loadratingpage() {
        SharedPreferences sPref;
        sPref = getPreferences(MODE_PRIVATE);
        return sPref.getInt("RATING_PAGE_NUMBER", 0);

    }

    /**
     * Fragment page adapter
     */
    private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {


        int Mode;
        int CP;
        public MyFragmentPagerAdapter(FragmentManager fm, int M,int PN) {
            super(fm);
            this.CP =PN;
            this.Mode=M;


        }

        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(position, this.Mode, this.CP);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    /**
     * Refreshing current page state
     */
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.updatescr) {
            MyGlobal myApp=(MyGlobal)this.getApplication();

            if (!myApp.getstate()) {

                if (check_connection()) {
                    PendingIntent pi;
                    Intent intent = new Intent(this, GetData.class);
                    pi = createPendingResult(1, intent, 0);
                    intent.putExtra(PARAM_SECTION, view_section).putExtra(PARAM_CURRENT_PAGE, current_page).putExtra(PARAM_BUTTON, 3).putExtra(PARAM_PINTENT, pi);

                    startService(intent);

                    mainpager = (ViewPager) findViewById(R.id.pager);
                    pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(), SECTION_DOWNLOAD, 0);
                    mainpager.setAdapter(pagerAdapter);
                } else {
                    Toast.makeText(this,getResources().getString(R.string.toast_no_connection), Toast.LENGTH_SHORT).show();
                }
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Saving current Section and page number
     * @param outState
     */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("GMode", view_section);
        outState.putInt("GCurPage", current_page);

    }

    /**
     * Loading saved Section and page number
     * @param savedInstanceState
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        view_section = savedInstanceState.getInt("GMode");
        current_page = savedInstanceState.getInt("GCurPage");

    }
    @Override
    /**
     * applying result from GetDAta thread
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == STATUS_RDONE) {
            view_section=data.getIntExtra(PARAM_SECTION, 0);
            current_page=data.getIntExtra(PARAM_CURRENT_PAGE, 0);

            if (view_section==SECTION_NEW) savelastnewpage(Integer.valueOf(current_page));
            if (view_section==SECTION_RATING) savelastratingpage(Integer.valueOf(current_page));

           MyGlobal myApp=(MyGlobal)this.getApplication();
            myApp.setstate(false);
            myApp.setpagenew(current_page);

            mainpager = (ViewPager) findViewById(R.id.pager);
            pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),view_section,current_page);
            mainpager.setAdapter(pagerAdapter);


        }
        if (resultCode == STATUS_RFAULT) {
            view_section=data.getIntExtra(PARAM_SECTION, 0);
            current_page=data.getIntExtra(PARAM_CURRENT_PAGE, 0);

                if (current_page==0) {
                    if (view_section == SECTION_NEW) current_page = loadnewpage();
                    if (view_section == SECTION_RATING) current_page = loadratingpage();
                }
            MyGlobal myApp=(MyGlobal)this.getApplication();
            myApp.setstate(false);
            myApp.setpagenew(current_page);

            mainpager = (ViewPager) findViewById(R.id.pager);
            pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),view_section,current_page);
            mainpager.setAdapter(pagerAdapter);


        }
    }
}

