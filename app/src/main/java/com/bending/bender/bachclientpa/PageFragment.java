package com.bending.bender.bachclientpa;
/**
 * Created by Bender on 009 09.10.14.
 * Page Fragment adapter
 */

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import android.widget.BaseAdapter;
import android.widget.Button;

import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class PageFragment extends Fragment {

    /**
     * listview with jokes
     */
    private ListView listview;
    public final static String PARAM_SECTION = "Section";
    public final static String PARAM_CURRENT_PAGE = "CurrentPage";
    public final static String PARAM_BUTTON = "Button";
    public final static String PARAM_PINTENT = "pendingIntent";
    private Button bnext;
    private Button bprev;
    private TextView twcurpage;
    PagerAdapter pagerAdapter;
    static final String ARGUMENT_PAGER_SCREEN = "pager_screen";
    static final String ARGUMENT_SECTION = "arq_section";
    BashBaseAdapter jokesDB;
    Cursor cJokes;
    Cursor cJokes2;
    int pager_screen;
    /**
     * Current section
     */
    int Section;
    /**
     * Current page number
     */
    int pagenumber;
    /**
     * Link to attached comics
     */
    private String simagelink;
    /**
     * Main pager
     */
    ViewPager pager;




    /**
     * posting positive or negative mark
     */
    public class PostThread extends AsyncTask<String, Void, String> {
        String referrer;
        String ratingUrl;
        String id;
        String sst;
        int Type;

        public PostThread (String src, String url,String id,int Type){
            this.referrer=src;
            this.ratingUrl=url;
            this.id=id;
            this.Type=Type;

        }

        @Override
        protected String doInBackground(String... arg) {

            try {
                if (Type==1)
                {
                    Document doc= Jsoup.connect(getResources().getString(R.string.Bash_url)+ratingUrl).data("act","rulez").data("quote",id).followRedirects(false).referrer(referrer).userAgent("Opera/9.80 (Windows NT 6.2; WOW64) Presto/2.12.388 Version/12.14").method(Connection.Method.POST).post();

                    sst = doc.text();
                }
                else
                {
                    Document doc= Jsoup.connect(getResources().getString(R.string.Bash_url)+ratingUrl).data("act","sux").data("quote",id).followRedirects(false).referrer(referrer).userAgent("Opera/9.80 (Windows NT 6.2; WOW64) Presto/2.12.388 Version/12.14").method(Connection.Method.POST).post();
                    sst = doc.text();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }



            return null;
        }

        @Override
        protected void onPostExecute(String result) {


        }
    }

    /**
     * Getting image thread
     */

    public class GetImageThread extends AsyncTask<String, Void, String> {

        String link;

        /**
         *
         * @param link comics image link
         */
        public GetImageThread(String link)
        {
            this.link=link;
        }
        @Override
        protected String doInBackground(String... arg) {

            try {

                Document doc = Jsoup.connect(getResources().getString(R.string.Bash_url) + link).get();
                Elements etemp = doc.select("#cm_strip");
                simagelink=etemp.attr("src");
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            ShowImage();
        }

    }

    /**
     *  Show image activity
     */
    public void ShowImage()
    {
        Intent intentIM = new Intent(getActivity(), PinchZoomPanActivity.class);
        intentIM.putExtra("imagelink", simagelink);
        startActivity(intentIM);
    }

    private  MyJokeListAdapter JokeListAdapter;

    private  MyMenuListAdapter MenuListAdapter;

    /**
     * Second page of Page Adapter with list
     */
    public class MyMenuListAdapter extends BaseAdapter implements View.OnClickListener {

        private final ArrayList<String> list;
        private final Context _context;
        private TextView   tvTextMenu;
        public MyMenuListAdapter(Context context )
        {
            super();
            this._context = context;
            this.list = new ArrayList<String>();
            list.clear();

            list.add(getResources().getString(R.string.Section_new));
            list.add(getResources().getString(R.string.Section_random));
            list.add(getResources().getString(R.string.Section_best));
            list.add(getResources().getString(R.string.Section_abyss));
            list.add(getResources().getString(R.string.Section_rating));


        }
        @Override
        public int getCount()
        {
            return list.size();
        }
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        @Override
        public void onClick(View view)
        {


        }

        public String getItem(int position)
        {
            return list.get(position);
        }
        public View getView(int position, View convertView, ViewGroup parent) {

            View row = convertView;

            if (row == null) {
                // ROW INFLATION
                LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.menuitem, parent, false);
            }

            tvTextMenu = (TextView) row.findViewById(R.id.textView1);
            tvTextMenu.setText(getItem(position));
            if (position+1==Section) {
                 tvTextMenu.setBackgroundColor(Color.LTGRAY);
            }
            row.setId(position);
            return row;
        }
    }
    public class MyJokeListAdapter extends BaseAdapter implements View.OnClickListener
    {
        private final ArrayList<Joke> list;
        private final Context _context;
        private TextView tvTextJoke;
        private TextView tvTextJokeId;
        private TextView tvTextJokeRulez;
        private TextView tvTextJokeSux;
        private TextView tvTextJokeDT;
        private TextView tvTextJokeRating;
        private ImageView imageComics;

        @Override
        public void onClick(View view)
        {

        }


        View.OnClickListener oclShowImage = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork!=null) {
                    new GetImageThread(list.get((Integer) v.getTag()).JokeComics).execute();
                }
                 else
                {
                     Toast.makeText(v.getContext(), getResources().getString(R.string.toast_no_connection), Toast.LENGTH_SHORT).show();
                }
            }
        };

        View.OnClickListener oclRulex = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                 if (activeNetwork!=null) {
                     Toast.makeText(v.getContext(), list.get((Integer) v.getTag()).JokeiD + " Rulez", Toast.LENGTH_SHORT).show();
                     new PostThread(getResources().getString(R.string.Bash_url), "/quote/" + list.get((Integer) v.getTag()).JokeiD + "/rulez", list.get((Integer) v.getTag()).JokeiD, 1).execute();
                 }
                else
                {
                    Toast.makeText(v.getContext(), getResources().getString(R.string.toast_no_connection), Toast.LENGTH_SHORT).show();
                }

            }
        };
        View.OnClickListener oclSux = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork!=null) {

                    Toast.makeText(v.getContext(), list.get((Integer) v.getTag()).JokeiD + " Suxx", Toast.LENGTH_SHORT).show();
                    new PostThread(getResources().getString(R.string.Bash_url), "/quote/" + list.get((Integer) v.getTag()).JokeiD + "/sux", list.get((Integer) v.getTag()).JokeiD, 2).execute();
                }
                else
                {
                    Toast.makeText(v.getContext(), getResources().getString(R.string.toast_no_connection), Toast.LENGTH_SHORT).show();
                }
            }

        };








        //////////////////
        public MyJokeListAdapter(Context context, Cursor Cur )
        {
            super();
            this._context = context;
            this.list = new ArrayList<Joke>();
            list.clear();
            if (Cur.moveToFirst())
            {
                do {
                    Joke jAdd=new Joke(Cur.getString(1),Cur.getString(2),Cur.getString(3),Cur.getString(4),Cur.getString(5) );
                    list.add(jAdd);
                } while (Cur.moveToNext());
            }


        }

        @Override
        public int getCount()
        {
            return list.size();
        }
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        public Joke getItem(int position)
        {
            return list.get(position);
        }
        public View getView(int position, View convertView, ViewGroup parent)
        {

            View row = convertView;

            if (row == null)
            {
                LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.item, parent, false);
            }
            Joke dvalue = getItem(position);
            tvTextJoke = (TextView) row.findViewById(R.id.textJokeText);
            tvTextJokeId = (TextView) row.findViewById(R.id.textJokeId);
            tvTextJokeDT = (TextView) row.findViewById(R.id.textJokeDT);
            tvTextJokeRating =(TextView) row.findViewById(R.id.textJokeRating);
            imageComics = (ImageView) row.findViewById(R.id.ivComics);
            tvTextJokeRulez = (TextView) row.findViewById(R.id.textJokeRulez);
            tvTextJokeSux = (TextView) row.findViewById(R.id.textJokeSux);
            tvTextJoke.setText(dvalue.JokeText);
            tvTextJokeRulez.setText("  +   ");
            tvTextJokeSux.setText("     -   ");
            tvTextJokeDT.setText(" "+dvalue.JokeDT);
            tvTextJokeId.setText(" #"+dvalue.JokeiD);
            tvTextJokeRulez.setOnClickListener(oclRulex);
            tvTextJokeRulez.setTag(position);
            tvTextJokeSux.setOnClickListener(oclSux);
            tvTextJokeRating.setText(dvalue.JokeRating);
            tvTextJokeSux.setTag(position);
            imageComics.setTag(position);
            if (dvalue.JokeComics.length()>4)
            {
                imageComics.setImageResource(R.drawable.comics);
                imageComics.setVisibility(View.VISIBLE);
                imageComics.setOnClickListener(oclShowImage);
            }
            else
            {
                imageComics.setVisibility(View.INVISIBLE);
            }

            row.setId(position);
            return row;
        }

    }

    static PageFragment newInstance(int screen, int sectioni,int currentpagei) {
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGER_SCREEN, screen);
        arguments.putInt(ARGUMENT_SECTION, sectioni);
        arguments.putInt(PARAM_CURRENT_PAGE, currentpagei);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

   public View.OnClickListener oclPrev = new View.OnClickListener()
    {
       int PrevPage;
       boolean CanGoPrev=false;
        @Override
        public void onClick(View v) {

            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork==null) {
                PrevPage=pagenumber-1;
                cJokes2 = jokesDB.getData(Section,PrevPage);
                if (cJokes2.getCount()>0) CanGoPrev=true;
            }
            else
            {
                CanGoPrev=true;
            }

            if (CanGoPrev) {

                PendingIntent pi;
                Intent intent = new Intent(v.getContext(), GetData.class);
                pi = getActivity().createPendingResult(1, intent, 0);
                intent.putExtra(PARAM_SECTION, Section).putExtra(PARAM_CURRENT_PAGE, pagenumber).putExtra(PARAM_BUTTON,1).putExtra(PARAM_PINTENT, pi);
                getActivity().startService(intent);
                pager = (ViewPager) getActivity().findViewById(R.id.pager);
                pagerAdapter = new MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(),  MyActivity.SECTION_DOWNLOAD, 0);
                pager.setAdapter(pagerAdapter);
            }

        }
    };

    View.OnClickListener oclNext = new View.OnClickListener()
    {
        int NextPage;
        boolean CanGoNext=false;
        @Override
        public void onClick(View v) {

            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork==null) {
                NextPage=pagenumber+1;
                cJokes2 = jokesDB.getData(Section,NextPage);
                if (cJokes2.getCount()>0) CanGoNext=true;
            }
            else
            {
                CanGoNext=true;
            }
            if (CanGoNext) {


                PendingIntent pi;
                Intent intent = new Intent(v.getContext(), GetData.class);
                pi = getActivity().createPendingResult(1, intent, 0);
                intent.putExtra(PARAM_SECTION, Section).putExtra(PARAM_CURRENT_PAGE, pagenumber).putExtra(PARAM_BUTTON, 2).putExtra(PARAM_PINTENT, pi);
                getActivity().startService(intent);
                pager = (ViewPager) getActivity().findViewById(R.id.pager);
                pagerAdapter = new MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(), MyActivity.SECTION_DOWNLOAD, 0);
                pager.setAdapter(pagerAdapter);
            }

        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pager_screen = getArguments().getInt(ARGUMENT_PAGER_SCREEN);
        Section  = getArguments().getInt(ARGUMENT_SECTION);
        pagenumber = getArguments().getInt(PARAM_CURRENT_PAGE);


    }

    private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {


        int Section;
        int CurrentPage;
        public MyFragmentPagerAdapter(FragmentManager fm, int Section,int CurrentPage) {
            super(fm);
            this.CurrentPage=CurrentPage;
            this.Section=Section;




        }



        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(position, this.Section, this.CurrentPage);
        }

        @Override
        public int getCount() {
            return 1;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        if (pager_screen==1) {

             view = inflater.inflate(R.layout.fragment, null);
            ListView lvMain = (ListView) view.findViewById(R.id.listViewModes);
            MenuListAdapter = new MyMenuListAdapter(view.getContext());
            lvMain.setAdapter(MenuListAdapter);


            lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {


                    ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    if (activeNetwork==null) {
                        Toast.makeText(view.getContext(), getResources().getString(R.string.toast_no_connection), Toast.LENGTH_SHORT).show();

                    }
                        PendingIntent pi;
                        Intent intent = new Intent(view.getContext(), GetData.class);
                        pi = getActivity().createPendingResult(1, intent, 0);
                        intent.putExtra(PARAM_SECTION, position + 1).putExtra(PARAM_CURRENT_PAGE,0).putExtra(PARAM_BUTTON, 0).putExtra(PARAM_PINTENT, pi);
                        getActivity().startService(intent);
                        pager = (ViewPager) getActivity().findViewById(R.id.pager);
                        pagerAdapter = new MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(),6, 0);
                        pager.setAdapter(pagerAdapter);
                        pager.setCurrentItem(0);


                }
            });

        }
        else
        {

            if (Section<6) {
                view = inflater.inflate(R.layout.fragment2, null);
                jokesDB = new BashBaseAdapter(view.getContext());
                cJokes = jokesDB.getData(Section,pagenumber);
                listview = (ListView) view.findViewById(R.id.listView1);
                JokeListAdapter = new MyJokeListAdapter(view.getContext(), cJokes);
                listview.setAdapter(JokeListAdapter);
                bnext = (Button) view.findViewById(R.id.button1);
                bprev = (Button) view.findViewById(R.id.button2);
                bnext.setOnClickListener(oclPrev);
                bprev.setOnClickListener(oclNext);
                twcurpage = (TextView) view.findViewById(R.id.textCurPage);
                switch (Section) {
                    case MyActivity.SECTION_NEW:
                        twcurpage.setText(getResources().getString(R.string.Section_new) +" "+ String.valueOf(pagenumber));
                        bnext.setVisibility(View.VISIBLE);
                        bprev.setVisibility(View.VISIBLE);
                        break;
                    case MyActivity.SECTION_RANDOM:
                        twcurpage.setText(getResources().getString(R.string.Section_random));
                        bnext.setVisibility(View.INVISIBLE);
                        bprev.setVisibility(View.INVISIBLE);
                        break;
                    case MyActivity.SECTION_BEST:
                        twcurpage.setText(getResources().getString(R.string.Section_best));
                        bnext.setVisibility(View.INVISIBLE);
                        bprev.setVisibility(View.INVISIBLE);
                        break;
                    case MyActivity.SECTION_ABYSS:
                        twcurpage.setText(getResources().getString(R.string.Section_abyss));
                        bnext.setVisibility(View.INVISIBLE);
                        bprev.setVisibility(View.INVISIBLE);
                        break;
                    case MyActivity.SECTION_RATING:
                        twcurpage.setText(getResources().getString(R.string.Section_rating)+" " + String.valueOf(pagenumber));
                        bnext.setVisibility(View.VISIBLE);
                        bprev.setVisibility(View.VISIBLE);
                        break;
                }
            }
            else
            {

                view = inflater.inflate(R.layout.loadingdata, null);

            }


        }



        return view;
    }
}