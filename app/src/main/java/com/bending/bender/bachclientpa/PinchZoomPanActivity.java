package com.bending.bender.bachclientpa;

/*
 * Copyright (C) 2011-2012 Wglxy.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


/**
 * This activity displays an image in an image view and then sets up a touch event
 * listener so the image can be panned and zoomed.
 */

public class PinchZoomPanActivity extends Activity {

/**
 * onCreate
 */
    TextView  tv;
    ProgressBar pb;
	private String simagelink;


@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Intent intent = getIntent();
    simagelink = intent.getStringExtra("imagelink");
    
   
    
    FrameLayout.LayoutParams fp = new FrameLayout.LayoutParams (FrameLayout.LayoutParams.FILL_PARENT,
                                                                FrameLayout.LayoutParams.FILL_PARENT,
                                                                Gravity.CENTER | Gravity.CENTER);
    FrameLayout view = new FrameLayout(this);
    setContentView (view);

    ImageView imageView = new ImageView(this);
    tv = new TextView(this);
    pb = new  ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
    pb.setLayoutParams(new FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT,Gravity.RIGHT | Gravity.CENTER));
    tv.setLayoutParams(new FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.FILL_PARENT,
            FrameLayout.LayoutParams.FILL_PARENT,Gravity.CENTER|Gravity.BOTTOM));
    tv.setText(getResources().getString(R.string.Image_loading));

    view.addView(imageView, fp);
    view.addView(tv);
    view.addView(pb);
    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
    view.setOnTouchListener(new PanAndZoomListener(view, imageView, PanAndZoomListener.Anchor.CENTER));


    Picasso.with(this).load(simagelink).into(imageView, new Callback.EmptyCallback() {
        @Override public void onSuccess() {
            tv.setVisibility(View.GONE);
            pb.setVisibility(View.GONE);
        }
        @Override
        public void onError() {
            tv.setVisibility(View.GONE);
            pb.setVisibility(View.GONE);
        }
    });


}

} // end class
